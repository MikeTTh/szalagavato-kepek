package main

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"fmt"
	"github.com/NYTimes/gziphandler"
	"gopkg.in/hlandau/passlib.v1"
	"html/template"
	"io/ioutil"
	"math"
	"math/big"
	"net/http"
	"os"
	"path/filepath"
	"sort"
	"strconv"
	"strings"
	"time"
)

type User struct {
	Name string
	Pics struct {
		Old string
		New string
	} `json:"-"`
	Extra   string
	Passwd  string
	Cookies []int
}

func check(e error) {
	if e != nil {
		panic(e)
	}
}

var users = make(map[string]User)
var imagenum = 0
var indexT, userT, imageTemp, noimageTemp, mindenkiTemp, someoneTemp *template.Template

var maxSize int64 = 40 * 1024 //20MB

func searchFiles() {
	users = make(map[string]User)
	imagenum = 0
	err := filepath.Walk("./img", func(path string, info os.FileInfo, err error) error {
		if info.IsDir() {
			if info.Name() == "img" {
				return nil
			}
			name := info.Name()
			users[name] = User{name,
				struct {
					Old string
					New string
				}{Old: "", New: ""},
				"",
				"",
				[]int{}}
			filepath.Walk(path, func(path string, info os.FileInfo, err error) error {
				tmp := users[name]
				if info.IsDir() {
					return nil
				} else if info.Name() == "info.json" {
					js, err := ioutil.ReadFile(path)
					check(err)
					err = json.Unmarshal(js, &tmp)
					check(err)
					users[name] = tmp
					return nil
				} else if strings.Contains(info.Name(), "Old") {
					tmp.Pics.Old = path
					imagenum++
					users[name] = tmp
					return nil
				} else if strings.Contains(info.Name(), "New") {
					tmp.Pics.New = path
					imagenum++
					users[name] = tmp
					return nil
				}
				return nil
			})
			return nil
		}
		return nil
	})
	check(err)
}

func init() {
	var err error
	indexT, err = template.ParseFiles("index.template.html")
	userT, err = template.ParseFiles("user.template.html")
	imageTemp, err = template.ParseFiles("image.template.html")
	noimageTemp, err = template.ParseFiles("noimage.template.html")
	mindenkiTemp, err = template.ParseFiles("osszeskep.template.html")
	someoneTemp, err = template.ParseFiles("egyember.template.html")
	check(err)
	searchFiles()
	fmt.Println("Number of images: " + strconv.Itoa(imagenum))
}

type index struct {
	Imagenum int
	Users    map[string]string
}

func indexHandler(w http.ResponseWriter, r *http.Request) {
	switch r.URL.Path {
	case "/", "/index.html", "/index.php", "/index":
		userlist := make(map[string]string)
		for k := range users {
			userlist[k] = users[k].Name
		}
		i := index{imagenum, userlist}
		indexT.Execute(w, i)
	case "/style.css":
		http.ServeFile(w, r, "style.css")
	case "/favicon", "/favicon.png":
		http.ServeFile(w, r, "favicon.png")
	default:
		http.ServeFile(w, r, "404.html")
	}
}

func storeUser(id string, u User) {
	file, err := os.Create("img/" + id + "/info.json")
	check(err)
	defer file.Close()
	enc := json.NewEncoder(file)

	enc.Encode(u)
	users[id] = u
}

func loginHandler(w http.ResponseWriter, r *http.Request) {
	id := strings.Split(r.URL.Path, "/")[2]
	u := users[id]
	pass := r.FormValue("Password")
	switch r.Method {
	case "POST":
		_, err := passlib.Verify(pass, u.Passwd)
		if err != nil {
			fmt.Fprint(w, "BADPASS")
		} else {
			num, err := rand.Int(rand.Reader, big.NewInt(math.MaxInt32))
			check(err)
			key := int(num.Int64())
			u.Cookies = append(u.Cookies, key)
			http.SetCookie(w, &http.Cookie{Name: id, Value: strconv.Itoa(key), Expires: time.Now().Add(time.Hour * 24 * 30), Path: "/"})
			fmt.Fprint(w, "OK")
			storeUser(id, u)
		}

	}
}

func userHandler(w http.ResponseWriter, r *http.Request) {
	id := strings.Split(r.URL.Path, "/")[2]
	switch r.Method {
	case "GET":
		if u, exist := users[id]; exist {
			var buf bytes.Buffer
			buf.WriteString("<table>")
			if u.Pics.Old != "" {
				imageTemp.Execute(&buf, map[string]interface{}{"Image": u.Pics.Old})
			} else {
				noimageTemp.Execute(&buf, map[string]interface{}{"Image": id + "/Old"})
			}
			if u.Pics.New != "" {
				imageTemp.Execute(&buf, map[string]interface{}{"Image": u.Pics.New})
			} else {
				noimageTemp.Execute(&buf, map[string]interface{}{"Image": id + "/New"})
			}
			buf.WriteString("</table>")
			d := struct {
				Name    string
				PicHtml template.HTML
				Extra   string
			}{u.Name, template.HTML(buf.String()), u.Extra}
			userT.Execute(w, d)
		} else {
			http.ServeFile(w, r, "404.html")
		}
	case "POST":
		key := 0
		cookie, e := r.Cookie(id)
		if e == nil {
			key, _ = strconv.Atoi(cookie.Value)
		}
		auth := false
		for _, k := range users[id].Cookies {
			if k == key {
				auth = true
			}
		}
		if auth {
			err := r.ParseMultipartForm(maxSize)
			check(err)
			switch r.FormValue("Type") {
			case "Delete":
				os.Remove(r.FormValue("File"))
				searchFiles()
			case "Upload":
				file, _, err := r.FormFile("Data")
				check(err)
				defer file.Close()
				fileBytes, err := ioutil.ReadAll(file)
				check(err)
				filetype := http.DetectContentType(fileBytes)
				var filename bytes.Buffer
				if strings.Contains(filetype, "image") {
					filename.WriteString("img/")
					filename.WriteString(r.FormValue("File"))
					filename.WriteString(strconv.Itoa(int(time.Now().Unix())))
					filename.WriteString(".")
					filename.WriteString(strings.Split(filetype, "/")[1])
					newFile, err := os.Create(filename.String())
					_, err = newFile.Write(fileBytes)
					check(err)
					go searchFiles()
				}
			}

			fmt.Fprint(w, "OK")
		} else {
			fmt.Fprint(w, "UNAUTH")
		}
	}
}

type sumUser struct {
	Id   string
	Name string
	Old  template.HTML
	New  template.HTML
}

func summaryHandler(w http.ResponseWriter, r *http.Request) {
	var buf bytes.Buffer
	var ids []string
	for id := range users {
		ids = append(ids, id)
	}
	sort.Strings(ids)
	for _, id := range ids {
		u := users[id]
		cur := sumUser{"", "", "", ""}
		if u.Pics.Old == "" {
			cur.Old = "<p>❌</p>"
		} else {
			var b bytes.Buffer
			b.WriteString("<img src='/")
			b.WriteString(u.Pics.Old)
			b.WriteString("'id='")
			b.WriteString(u.Pics.Old)
			b.WriteString("' width='300vw' onclick='imageZoom(id)'")
			cur.Old = template.HTML(b.String())
		}
		if u.Pics.New == "" {
			cur.New = "<p>❌</p>"
		} else {
			var b bytes.Buffer
			b.WriteString("<img src='/")
			b.WriteString(u.Pics.New)
			b.WriteString("'id='")
			b.WriteString(u.Pics.New)
			b.WriteString("' width='300vw' onclick='imageZoom(id)'")
			cur.New = template.HTML(b.String())
		}
		cur.Name = u.Name
		cur.Id = id
		someoneTemp.Execute(&buf, cur)
	}
	mindenkiTemp.Execute(w, struct {
		Table template.HTML
	}{template.HTML(buf.String())})
}

func imgHandler(w http.ResponseWriter, r *http.Request) {
	if strings.Contains(r.URL.Path, "info.json") {
		http.ServeFile(w, r, "404.html")
		return
	}
	http.ServeFile(w, r, r.URL.Path[1:])
}

func redirect(w http.ResponseWriter, r *http.Request) {
	target := "https://" + strings.Split(r.Host, ":")[0] + r.URL.Path
	http.Redirect(w, r, target, http.StatusTemporaryRedirect)
}

func main() {
	mux := http.NewServeMux()
	mux.HandleFunc("/img/", imgHandler)
	mux.HandleFunc("/user/", userHandler)
	mux.HandleFunc("/login/", loginHandler)
	mux.HandleFunc("/mindenki/", summaryHandler)
	mux.HandleFunc("/", indexHandler)
	go http.ListenAndServe(":8080", http.HandlerFunc(redirect))
	gz := gziphandler.GzipHandler(mux)
	err := http.ListenAndServeTLS(":443", "fullchain.pem", "privkey.pem", gz)
	check(err)
}
