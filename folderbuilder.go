package main

import (
	"bytes"
	"crypto/rand"
	"encoding/json"
	"gopkg.in/hlandau/passlib.v1"
	"math"
	"math/big"
	"os"
	"regexp"
	"strconv"
	"strings"
	"unicode"

	"golang.org/x/text/transform"
	"golang.org/x/text/unicode/norm"
)

func check(e error) {
	if e != nil {
		panic(e)
	}
}

func isMn(r rune) bool {
	return unicode.Is(unicode.Mn, r) // Mn: nonspacing marks
}

func main() {
	nameString := ` 1.	Bernáth Róbert	 16.	Mácsek Áron Bence
 2.	Berta Gabriella	 17.	Nagy-Pál Dorottya
 3.	Cséby Róza Bella	 18.	Nyárádi Dániel
 4.	Denkler Jorgosz Dominik	 19.	Rácz Levente
 5.	Dobosi Péter László	 20.	Remete Adrián
 6.	Füle Márton	 21.	Szántó Virág Veronika
 7.	Gachályi Mátyás Miksa	 22.	Tavaszi Anna
 8.	Gárdos Tamás	 23.	Tóth Dzsenifer
 9.	Horváth Dominik Iván	 24.	Tóth Martin László
 10.	Horváth Erik Ernő	 25.	Tóth Miklós Tibor
 11.	Hosszú Kristóf	 26.	Vadász Virág
 12.	Iglódi Emese Kinga	 27.	Varga Ákos
 13.	Juhász Gergely László	 28.	Varga Eszter
 14.	Katona Lilla	 29.	Varga Sára
 15.	Kator Balázs`
	var names []string
	name := ""
	match := false
	prevMatch := false
	for _, c := range nameString {
		prevMatch = match
		match, _ = regexp.MatchString(`[\pL]`, string(c))
		if match {
			name += string(c)
		} else if prevMatch {
			name += string(c)
		} else {
			if len(name) > 0 {
				names = append(names, strings.Replace(strings.Replace(name, "\t", "", -1), "\n", "", -1))
				name = ""
			}
		}
	}

	type User struct {
		Name   string
		Extra  string
		Passwd string
	}

	var out bytes.Buffer

	for _, n := range names {
		t := transform.Chain(norm.NFD, transform.RemoveFunc(isMn), norm.NFC)
		id, _, _ := transform.String(t, n)
		id = strings.ToLower(id)
		id = strings.Replace(id, " ", "", -1)
		num, err := rand.Int(rand.Reader, big.NewInt(math.MaxInt32))
		passw := strconv.Itoa(int(num.Int64()))
		out.WriteString(n)
		out.WriteString(": ")
		out.WriteString(passw)
		out.WriteString("\n")
		check(err)
		hash, _ := passlib.Hash(passw)
		u := User{n, "", hash}
		err = os.MkdirAll(`img/`+id, os.ModePerm)
		check(err)
		infoFile, err := os.Create(`img/` + id + `/info.json`)
		defer infoFile.Close()
		check(err)
		enc := json.NewEncoder(infoFile)
		enc.Encode(u)
	}
	passFile, err := os.Create("passwords.txt")
	check(err)
	defer passFile.Close()
	passFile.WriteString(out.String())
}
